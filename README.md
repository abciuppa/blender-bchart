Bchart
======
Bchart is the best way to show your data in simple and animated charts.

![](./images/Logo.jpg)

5+ 2D chart types (and variants)
======

![](./images/Risorsa_3.png)


Main features
======

Import .csv data into Blender and create/remove charts;

![](./images/Risorsa_9.png)


Change chart type at anytime;

![](./images/Risorsa_10.png)


Tweak more than 30 parameters to create the best looking chart ever.

![](./images/Risorsa_11.png)


The chart has an origin point to move, rotate and scale it.

![](./images/Risorsa_13.png)


Add animation in one click.

![](./images/Risorsa_12.png)


Anatomy of a chart

![](./images/Risorsa_1.png)

Chart elements



Interface
======

![](./images/main_panel.png)

Bchart main panel

Dataset structure
======

![](./images/Risorsa_2.png)

Data has to be organized in column.

Each column represents a dataset: while the first line contains the name of the dataset; each subsequent row can be populated with data.

The first column on the left will be considered as 'X axis' data, but any other column can be chosen at any time in the add-on settings. Just remember that you can only use one column/dataset at a time as a source for "x axis labels".

Important notes
======
Do not change the name of the objects, this would make them unrecognizable to the addon.

The name of the .csv file is used as the name of the chart. Currently it cannot be changed, but I am working on making it possible.

Currently it only supports numeric values into the "plot area". That said, you can still use textual data for dataset used as "x axis values" (Into the first column by default, but also in the others if you want).

In the future
======
I am working on a style system, and my idea is to add new types of charts, animations and supported data sources in addition to .csv: you can give suggestions by contacting me. I will reply as soon as possible.



Links and contacts:
========
Purchase:

-   [Gumroad](https://gumroad.com/l/bchart)
-   [BlenderMarket]()

Docs:
-   [Project page](https://gitlab.com/abciuppa/blender-bchart)
-   [Documentation](https://gitlab.com/abciuppa/blender-bchart/wikis/Documentation)

Contacts
-   abciuppa@gmail.com