Charts
==================================

.. figure:: /images/All_charts.png
    :scale: 35 %
    :align: center

    All the supported charts

.. toctree::
   :maxdepth: 2

   2d charts.rst
   3d charts.rst
