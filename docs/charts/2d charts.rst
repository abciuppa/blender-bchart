
2D charts
==================================

Bchart is a *blender addon* focused on the visual representation of data.
It takes advantage of the Blender's features to generate customizable graphics, using both Grease Pencil and mesh objects.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Line <line 2d>
   Area <area 2d>
   Bar <bar 2d>
   Histogram <histogram 2d>
   Pie <pie 2d>
