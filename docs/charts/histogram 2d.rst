
Histogram 2D
==================================

Bchart is a *blender addon* focused on the visual representation of data.
It takes advantage of the Blender's features to generate customizable graphics, using both Grease Pencil and mesh objects.

