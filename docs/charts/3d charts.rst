
3D charts
==================================

Bchart is a *blender addon* focused on the visual representation of data.
It takes advantage of the Blender's features to generate customizable graphics, using both Grease Pencil and mesh objects.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Bar <bar 3d>
   Histogram <histogram 3d>
   Pie <pie 3d>
