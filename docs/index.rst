.. bchart documentation master file, created by
   sphinx-quickstart on Tue Sep  8 21:30:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Bchart reference manual
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. .. figure:: /images/Logo.jpg
..     :scale: 40 %
..     :align: center

..     BCHART


.. toctree::
   :maxdepth: 1
   :caption: Bchart:
   :hidden:
    
   about/index.rst
   anatomy/index.rst
   charts/index.rst
   examples/index.rst
.. animation.rst

.............................................................................


   .. figure:: /images/Logo.jpg
      :align: center
      :target: about/index.html

   :doc:`/about/index`
      An introduction to the **Bchart** addon.

.............................................................................


   .. figure:: /images/Risorsa_1.png
      :scale: 35 %
      :align: center
      :target: anatomy/index.html

   :doc:`/anatomy/index`
      The components of a chart.
      
.............................................................................


   .. figure:: /images/All_charts.png
      :scale: 35 %
      :align: center
      :target: charts/index.html

   :doc:`/charts/index`
      A list of the supported charts and parameters.
      
.............................................................................


   .. figure:: /images/Logo.jpg
      :align: center
      :target: examples/index.html

   :doc:`/examples/index`
      Some examples and *how-to*.
      
.............................................................................


   .. figure:: /images/Logo.jpg
      :align: center
      :target: examples/index.html

   :doc:`/cookbook/index`
      Smart Bchart cookbook.
      
.............................................................................


..   .. container:: descr

..      .. figure:: /images/Logo.jpg
..         :target: animation.rst

..      :doc:`/animation`
..         Overview of the interface and functionality of all editors.


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
