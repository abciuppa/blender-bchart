.. _prepare data:

Prepare the data
==================================

Data format
**********************************

.. | **Datasets** are the core objects of the whole system. A dataset is a single column of data, composed by a *name* and a list of *values*.
.. | Assuming that, there are no known limits neither in the number of datasets nor in values rows, except your PC's capabilities to manage Blender calculation while running the addon.


| By default the addon display a linear scale along the x axis: this means that labels through [0, n] range wil be displayed, where n is the maximum number of values in your datasets (alias rows). But you may need a custom scale along the x axis, such as the logarithmic one, or you may simply feel unsatisfacted with the default behaviour. In that case you can specify your scale values into the first column: this will be used as custom x-label scale.
| As a suggestion, each one of the columns can be later selected as the "x-axis" one. The use the first one is only a standard.

**TODO: aggiungere immagine dell'asse x con i label di default**

In summary, Your spreadsheet structure should look similar to this image before exporting into .csv format. Be aware that datasets must be in numeric format. Also, you can change the column for x-axis later, the first column is taken as default.

.. figure:: /images/Risorsa_2.png
    :scale: 35 %
    :align: center

**Make shure that the names of the datasets are unique.**


Data exports
**********************************

The .csv data can be separated by both comma and semicolons, as the example below. Semicolon are recommended because some regions across the world use the comma symbol for decimal numbers, causing errors while importing the data.

.. figure:: /images/csv_example.jpg
    :scale: 100 %
    :align: center

