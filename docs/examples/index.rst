Examples
==================================

Here you can find some practical examples of the charts.


.. toctree::
   :maxdepth: 1

   prepare the data.rst
   examples.rst

