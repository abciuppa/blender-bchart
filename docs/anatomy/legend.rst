Legend
**********************************

| By default the legend is placed at the bottom of the chart and aligned centered, but by tweaking the parameters it can be placed everywhere.
| It has a bounding-box to simplify its styling, and by changing its width the entries will be fitted inside, regarding all the other parameters such as spacings and entries type.

**TODO: aggiungere immagine di esempio dei parametri della legenda + estratto di legenda da un grafico**
