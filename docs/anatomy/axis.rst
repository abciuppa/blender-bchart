Axis
**********************************

| Obviously, each 2d chart has 2 main axes: *x axis* and *y axis*. The only exception are the pie/donut charts, which are described below. Generally, the axes will be referred as **categories** and **values** respectively.
| Moreover, 3D charts has an additional axis, the *z axis* also called the **series axis**.

For each axis, two labels types are provided: *axis title* and *axis values*.
The parameters available are pretty the same and concern the positioning and rotation of text objects into the 3d space and their color, font and size styling. Also, each one of the following can be shown/hidden independently based on the visualization needs.

**TODO: aggiungere immagine di esempio dei parametri degli assi (titoli e valori)**


X axis - Categories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The **categories** represent specific classes of dataset, and by default they are visualized along the x axis. Each chart type has its own categories. For example, while the Bar chart shows dataset names, the Histogram chart shows values ranges that depends on the number of bins.

Categories shown for each chart:

* *Line*: progressive [0, n] numbers or custom numbers if a dataset is marked as "x_label" type.
* *Area*: progressive [0, n] numbers or custom numbers if a dataset is marked as "x_label" type.
* *Histogram*: values vary depending on the number of bins.
* *Bar*: if columns are :ref:`grouped <cookbook bar group>` "by dataset" then the dataset names are use as labels under each group; otherwise if columns are :ref:`grouped <cookbook bar group>` "by row" labels show progressive [0, n] numbers (number of datasets).
* *Pie*: dataset names, centered in the circular sector spanned by each dataset slice.

**TODO: aggiungere immagine di esempio dell'asse x per ciascun tipo di grafico**

.. Categories labels
.. """"""""""""""""""""""""""""""""""
.. The 


Y axis - Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The **values** are numeric data shown on the y axis. The maximum and minimum values along the axis can be independently increased or decreased to scale up or scale down the height of the chart.

**TODO: aggiungere immagine dell'asse y e dei parametri che modificano il range di valori e ne scalano le dimensioni in altezza**

.. Values labels
.. """"""""""""""""""""""""""""""""""
.. todo


Z axis - Series
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3D charts have a third axis, that represents the depth of the chart. This additional dimension is used to arrange or group the datasets differently, and add more informations to the graphs.

**TODO: aggiungere immagine del parametro della profondità, con l'esempio di un grafico**
