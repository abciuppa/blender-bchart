Anatomy of a chart
==================================

There are some basic elements that can be found in all the graphics: these are the main objects that make them up.

.. figure:: /images/Risorsa_1.png
    :scale: 35 %
    :align: center

    Main components of the chart


.. toctree::
   :maxdepth: 1

   plot_area.rst
   axis.rst
   legend.rst
   animation.rst

