Plot area
**********************************

The printing area is the main space, where the chart is shown.


Dataset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **Datasets** are the core objects of the whole system. A dataset is a single column of data, composed by a *name* and a list of *values*.
| Assuming that, there are no known limits neither in the number of datasets nor in values rows, except your PC's capabilities to manage Blender calculation while running the addon.

See :ref:`Prepare the data <prepare data>` for a full example of data format.
