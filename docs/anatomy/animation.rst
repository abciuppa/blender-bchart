Animation
**********************************

Animations are there to take even more advantage of Blender's features.
They are probably the simplest thing you can with this addon, but despite this they are very powerful.

How to
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Just set the start and end frames, check if you need to animate labels too and :ref:`refresh <cookbook refresh>` the chart. 

**TODO: aggiungere immagine dei parametri dell'animazione**

**TODO: aggiungere immagine .gif con un grafico animato**