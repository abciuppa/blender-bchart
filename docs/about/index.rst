About
==================================

What is Bchart?
**********************************

Bchart is the best way to show your data in simple and animated graphics.

It is a *blender addon* focused on the visual representation of data.
It takes advantage of the Blender's features to generate customizable graphics, using both Grease Pencil and mesh objects.


This `video <https://www.youtube.com/watch?v=-yXpGdodbnU>`_ shows the main features of the addon.


Features
**********************************

#. Importing of **.csv data**
#. **Dynamic** chart editing
#. More than **30** parameters
#. Charts can be moved, rotated, scaled at any time
#. Add **animation**



Chart types
**********************************

.. figure:: /images/Risorsa_3.png
    :scale: 50 %
    :align: center




.. .. toctree::
..    :maxdepth: 1

..    introduction.rst

